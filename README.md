# HM2-Ubuntu-Ports

#### 介绍
为红米2移植了基于6.6.0kernel的Ubuntu20.04系统镜像。可以安装ROS, 内核支持CAN, 可以使用docker！
想要自己编译移植内核请参考[这个](https://gitee.com/meiziyang2023/hm2-ubuntu-ports/blob/master/%E7%BC%96%E8%AF%91%E6%95%99%E7%A8%8B.md)


![图片示例](image.png)

#### 安装教程

1.  将你的手机刷机到安卓5.0以上的小米官方内核，请使用小米官方刷机助手。
2.  下载本仓库的[发行版](https://gitee.com/meiziyang2023/hm2-ubuntu-ports/releases/tag/kenel6.6.0rc1)分卷压缩包并解压
3.  手机按住 音量- 开机，进入fastboot模式
3.  运行压缩包中的一键刷机脚本即可

#### 使用说明

1.  通过otg转接器连接键盘.
2.  输入用户名`umeko`回车，输入密码`1234`回车登录终端（输入密码时屏幕不会显示内容，但是是在输入的）
3.  使用命令连接wifi：`sudo nmcli dev wifi connect WIFI名称 password WIFI密码`
4.  机器默认开启了SSH服务，用户名`umeko`密码`1234`，愉快的玩耍吧。

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
